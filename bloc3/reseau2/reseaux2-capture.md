# Capture de paquets réseaux

Le but de cette activité est de capturer des paquets sur le réseau à l'aide
de l'outil `wireshark` et de constater que, sans précautions particulières,
les données sont effectivement transmises en clair entre navigateur et site web.

La manipulation s'effectue à l'aide d'une machine virtuelle VirtualBox abritant
une distribution desktop Ubuntu 18.04.2. Un serveur `apache2` y est installé,
la page par défaut du serveur (`/var/www/html/index.html`) a été remplacée
par une page contenant un simple formulaire HTML de connexion. Pour information,
le document [suivant](creer-vm.md) décrit comment a été créée cette machine virtuelle.

 Lancement de la machine virtuelle
 ---
  - l'image disque de la machine virtuelle est disponible dans `/local/iso/ubuntu-reseaux2.vdi` (ous sous forme compressée `bz2`)
  - copiez cette image dans votre répertoire **local** personnel
  ```
  cd /local/login/
  mkdir vdiimages
  cp /local/iso/ubuntureseaux2.vdi /local/login/vdiimages/
  ```
  et décompressez la (éventuellement)
  ```
  bunzip2 /local/login/vdiimages/ubuntureseaux2.vdi.bz2
  ```
  - lancez VirtualBox et créez une nouvelle machine `ubuntureseaux2`
    - dans la partie `Disque dur` sélectionnez `Utiliser un fichier de disque dur virtuel existant` et désignez votre copie de l'image créée ci-dessus
    - lancez la machine virtuelle `ubuntureseaux2`
  - connectez-vous à l'aide de l'identifiant user/password `localuser/localuser`

Capture des paquets réseau
---
  - lancez un terminal et passez `root` avec la commancde `sudo -i`
  - lancez `wireshark`, sélectionnez l'interface `loopback` et démarrez la capture sur cette interface (cliquez sur le petit aileron bleu)
  - par ailleurs lancez `firefox` dans la machine virtuelle et ouvrez l'url `localhost`
  dans ce navigateur: le formuaire HTML de connexion doit s'afficher.
  - Entrez alors un utilisateur/mot de passe quelconque dans le formulaire et soumettez
  le formulaire avec le bouton `Se connecter` du formulaire
  - observez l'apparition des paquets dans `wireshark`. Vous pouvez interrompre la capture (petit carré rouge) et examiner en détail le paquet qui correspond à cet envoi.
  Comme l'illustre la figure ci-dessous, les deux champs `user` et `passwd` du formulaire
  apparaissent en clair.
  ![](figs/reseaux2-capture.png)

Utilisation du protocole `https`
---
  - utilisez maintenant l'url de connexion `https://localhost` dans `firefox`:
  celui-ci doit afficher un page d'avertissement concernant un risque de sécurité
  (certificat auto-signé)
  - cliquez sur `Advanced`  pour faire apparaître le bandeau permettant de passer
  outre cette alerte de sécurité (`Accept the risk and Continue`).
  - vous êtes à nouveau sur le formulaire de connexion en mode sécurisé (ou presque)
  - relancez une capture dans `wireshark` sur l'interface `localhost`
  - connectez vous sur le formulaire avec un user/password quelconque
  - arrétez la capture dans `wireshark` et observez les paquets échangés.
  Le protocole `https` utilise des paquets supplémentaires lors des échanges (`Client Hello`, `Server Hello`, ...) en plus des paquets contenant les données. Les paquets de données sont affichés comme `Application Data` par `wireshark` et
  l'examen de leurs contenus illustre bien le fait que les données (ici `user` et `passwd`) ont été cryptées.
  ![](figs/reseaux2-ssl-capture.png)

Utiliser plusieurs machines virtuelles
---
  - Il est possible cloner une machine virtuelle dans VirtualBox à l'aide du menu contextuel.
  Choisissez `Clonage intégral` afin que le fichier associé à la machine virtuelle soit copié (et non simplement lié).
  - Vous pouvez modifier les configurations de vos VMs en diminuant la taille mémoire utilisée (passage de 4Gio à 2Gio par exemple).
  - VirtualBox permet de faire communiquer les machines virtuelles entre elles à l'aide d'un commutateur (switch) virtuel. C'est le mode réseau `NAT service` (`Réseau NAT`).
  Il faut commencer par créer un tel réseau avant de pouvoir y connecter les machines virtuelles. Cele se réalise sur la machine hôte par une commande VirtualBox:
  ```
  VBoxManage natnetwork add -t nat-int-network -n "192.168.15.0/24" -e -h on
  ```
  Nous créons ici un réseau NAT nommé `nat-int-network` d'adresse `192.168.15.0/24`,
  la passerelle par défaut étant `192.168.0.1`,
  l'option `-h on` y affecte automatiquement un serveur DHCP (vous n'aurez pas besoin de configurer manuellement le réseau dans vos machines virtuelles).
  - une fois le réseau NAT  créé, vous pouvez modifier dans VirtualBox les configurations réseau de vos VMs en les passant en `Réseau NAT` et en spécifiant le nom du réseau défini ci-dessus (`nat-int-network`)
  - Lancez la première machine virtuelle, connectez-vous et affichez son adresse IP
  - *Attention*: avant de lancer la seconde machine virtuelle modifier son adresse MAC dans la configuration Réseau `Avancé` de VirtualBox, sinon le serveur DHCP leur affectera la même adresse IP. Profitez en pour également autoriser le mode `Promiscuité` si vous voulez capturer des paquets sur cette interface réseau.
  - Lancez la seconde machine virtuelle et affichez son adresse IP.
  - Effectuez des pings entre vos deux machines virtuelles.
  - Testez l'accès vers l'extérieur.
  - Vous pouvez également désactiver le serveur DHCP, redémarrer vos machines virtuelles
  et y configurer manuellement (pour une fois seulement) le réseau à l'aide de la commande `ip`. Sur la machine hôte, lancez
  ```
  VBoxManage natnetwork modify --netname nat-int-network --dhcp off
  ```
  Puis après avoir démarré les machines virtuelles, utilisez la commande `ip`.
  - Faites le ménage quand vous avez terminé:
  ```
  VBoxManage natnetwork remove --netname nat-int-network
  ```
