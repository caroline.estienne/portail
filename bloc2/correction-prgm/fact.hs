-- Exemple en Haskell de la même décomposition de factoriel. Ici la sémantique
-- opérationnelle étant paresseuse, le programme se comporte comme le suggère
-- l'intuition. Notez cependant que la vaste majorité des langages de
-- programmation sont stricts et ont le même comportement que Python sur cet
-- exemple.

fact n =
  if n==0
  then 1
  else n* fact(n-1)

zero n p =
  if n==0
  then 1
  else p

fact2 n = zero n (n * fact2 (n-1))
